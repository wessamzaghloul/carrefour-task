import * as validate from "validate.js";
import _ from "lodash";

export function init() {
    var forms = document.querySelectorAll("form[validation]");
    if (forms.length) {
        [].map.call(forms, form => {
            var inputs = form.querySelectorAll(
                "input[name], select[name], textarea[name]"
            );
            if (inputs.length) {
                var constraints = {};

                [].map.call(inputs, input => {
                    var key = input.getAttribute("name");
                    constraints[key] = {};
                    if (input.dataset.validatePresence) {
                        let obj = JSON.parse(input.dataset.validatePresence) || {};
                        constraints[key]["presence"] = obj;
                    }
                    if (input.dataset.validateEmail) {
                        let obj = JSON.parse(input.dataset.validateEmail) || {};
                        constraints[key]["email"] = obj;
                    }

                    if (input.dataset.validateLength) {
                        let obj = JSON.parse(input.dataset.validateLength) || {};
                        constraints[key]["length"] = obj;
                    }
                    if (input.dataset.validateFormat) {
                        let obj = JSON.parse(input.dataset.validateFormat) || {};
                        constraints[key]["format"] = obj;
                    }
                });

            }

            form.addEventListener("submit", function (e) {
                e.preventDefault();
                handleFormSubmit(form, constraints, showSuccess);
            });

            for (var i = 0; i < inputs.length; ++i) {
                inputs.item(i).addEventListener("change", function (ev) {
                    var errors = validate.validate(form, constraints, { fullMessages: false }) || {};
                    showErrorsForInput(this, errors[this.name]);
                });
            }

        })
    }



}



// These are the constraints used to validate the form

function handleFormSubmit(form, constraints, callback) {
    // validate the form against the constraints
    var errors = validate.validate(form, constraints, { fullMessages: false });
    // then we update the form to reflect the results
    showErrors(form, errors || {});
    if (!errors) {
        form.submit();
    }
}

// Updates the inputs with the validation errors
function showErrors(form, errors) {
    // We loop through all the inputs and show the errors for that input
    _.each(
        form.querySelectorAll("input[name], select[name], textarea[name]"),
        function (input) {
            // Since the errors can be null if no errors were found we need to handle that
            showErrorsForInput(input, errors && errors[input.name]);
        }
    );
}

// Shows the errors for a specific input
function showErrorsForInput(input, errors) {
    var formGroup = input.parentNode;
    // resets the classes
    resetFormGroup(input, formGroup);
    // If we have errors
    if (errors) {
        // we first mark the group has having errors

        input.classList.add("is-invalid");
        // then we append all the errors
        _.each(errors, function (error) {
            addError(input, error);
        });
    } else {
        // otherwise we simply mark it as success
        input.classList.remove("is-invalid");
        input.classList.add("is-valid");
    }
}


function resetFormGroup(input, formGroup) {
    // Remove the success and error classes
    input.classList.remove("is-invalid");
    input.classList.remove("is-valid");
    // and remove any old messages
    _.each(formGroup.querySelectorAll(".invalid-tooltip"), function (el) {
        el.parentNode.removeChild(el);
    });
}

function addError(input, error) {


    var block = document.createElement("div");
    block.classList.add("invalid-tooltip");
    block.innerText = error;

    input.parentNode.appendChild(block);
}

function showSuccess() {
    // We made it \:D/
    console.log("Success!");
}