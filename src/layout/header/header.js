export function init() {
    var searchForm = document.querySelector("header form[search-form]");
    if (searchForm) {
        
        toggleSearch(searchForm)

        document.addEventListener("click", function (e) {
            searchForm.classList.remove("active");
            var input = searchForm.querySelector("input");
            clearInput(input)
        });
        searchForm.addEventListener("click", function (e) {
            e.stopPropagation();
        });


    }
    var header = document.querySelector("header");
    if(header){
        addClasstoElementOnScroll(header);
    }
    
    
}

function toggleSearch(searchForm){
    var toggler = searchForm.querySelector("a");
    var input = searchForm.querySelector("input");
    toggler.addEventListener("click", function (e) {
        e.preventDefault();
        searchForm.classList.toggle("active");
        if (searchForm.classList.contains("active")) {
            input.focus();
        }
        else {
            clearInput(input)
        }

    });
}

function addClasstoElementOnScroll(element){
    window.addEventListener('scroll', function () {
       
        if (element.offsetTop !== 0) {
            if (!element.classList.contains('scrolled')) {
                element.classList.add('scrolled');
            }
        } else {
            element.classList.remove('scrolled');
        }

    });
}

function clearInput(input) {
    setTimeout(function () { input.value = ""; }, 500);
}
