//styles
import "./style.scss";
//jQuery
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
//bootstrap
import "bootstrap";

//layout
import "./layout/index";

//tiles
import "./tiles/index";

//widgets
import "./widgets/index";